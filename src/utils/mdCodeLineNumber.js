function codeLineNumber(tag) {
    let pre = document.getElementsByTagName(tag), pl = pre.length;
    for (let i = 0; i < pl; i++) {
        pre[i].innerHTML = '<span class="line-number"></span>' + pre[i].innerHTML + '<span class="cl"></span>';
        let lineCount = pre[i].innerHTML.split(/\n/).length;
        for (let j = 0; j < (lineCount - 1); j++) {
            let lineNum = pre[i].getElementsByTagName('span')[0];
            lineNum.innerHTML += '<span>' + (j + 1) + '</span>';
        }
    }
}
export { codeLineNumber };