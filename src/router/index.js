import { createRouter, createWebHistory } from 'vue-router'
import dashboardLayout from '../views/dashboard/index.vue'

const routes = [
  {
    path: '/',
    redirect:'/dashboard/blogs/list',
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login/Login.vue')
  },
  {
    path: '/dashboard',
    redirect:'/dashboard/blogs/list',
    name: 'Admin',
    component: dashboardLayout,
    children: [
      // BLOGS
      {
        path: 'blogs',
        redirect:'/dashboard/blogs/list',
      },
      {
        path: 'blogs/list',
        name: 'BlogsList',
        component: () => import('../views/dashboard/blogs/BlogsList.vue'),
      },
      {
        path: 'blogs/create',
        name: 'BlogCreate',
        component: () => import('../views/dashboard/blogs/BlogsEditor.vue'),
        // props: true,
      },
      {
        path: 'blogs/edit/:id',
        name: 'BlogEdit',
        component: () => import('../views/dashboard/blogs/BlogsEditor.vue'),
        // props: true,
      },

      // FOOTPRINTS
      {
        path: 'footprints',
        redirect:'/dashboard/footprints/list',
      },
      {
        path: 'footprints/list',
        name: 'FootprintsList',
        component: () => import('../views/dashboard/footprints/FootprintsList.vue'),
      },
      {
        path: 'footprints/create',
        name: 'FootprintCreate',
        component: () => import('../views/dashboard/footprints/FootprintsEditor.vue'),
      },
      {
        path: 'footprints/edit/:id',
        name: 'FootprintsEdit',
        component: () => import('../views/dashboard/footprints/FootprintsEditor.vue'),
        // props: true,
      },

      // TAGS
      {
        path: 'tags',
        name: 'Tags',
        component: () => import('../views/dashboard/Tags.vue'),
      },

      // SETTINGS
      {
        path: 'settings',
        name: 'Settings',
        component: () => import('../views/dashboard/Settings.vue'),
      },
    ]
  },
  {
    path: '/:catchAll(.*)',
    name: 'PageNotFound',
    component: () => import('../views/404.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
