import axios from 'axios'
import { message } from 'ant-design-vue';

// 创建axios实例
const service = axios.create({
  // 为防止 no-referrer-when-downgrade 先使用http
  baseURL: 'http://192.168.89.3:9192/dashboard',
  timeout: 5000,
  headers: {
    'content-type': 'application/json;charset=UTF-8',
  }
})

// 携带 cookie
service.defaults.withCredentials = true

// 拦截
service.interceptors.request.use(config => {
  return config;
}, error => {
  console.log(error);
  return Promise.reject(error);
})

// API
export const getEncap = async (url, params, funcSuccess, successText, funcFail, failText) => {
  let res;
  params = params || {};
  await service.get(url, {
    params: params
  }).then(result => {
    res = result;
  });

  if (res.data.code === 201) {
    // console.log('api:ok->' + JSON.stringify(res.data));
    if (funcSuccess) {
      funcSuccess(res.data);
    }
    if (successText !== null && successText !== '' && successText !== undefined) {
      message.success(successText);
    }
  } else {
    console.log('api:fail->' + JSON.stringify(res));
    if (funcFail) {
      funcFail(res.data);
    }
    if (failText !== null && failText !== '' && failText !== undefined) {
      message.error(failText);
    } else {
      message.error(res.message);
    }
  }
}

export const postEncap = async (url, data, funcSuccess, successText, funcFail, failText) => {
  let res;
  await service.post(url, data).then(result => {
    res = result;
  });

  if (res.data.code === 201) {
    // console.log('api:ok->' + JSON.stringify(res.data));
    if (funcSuccess) {
      funcSuccess(res.data);
    }
    if (successText !== null && successText !== '' && successText !== undefined) {
      message.success(successText);
    }
  } else {
    console.log('api:fali->' + JSON.stringify(res.data))
    if (funcFail) {
      funcFail(res.data);
    }
    if (failText !== null && failText !== '' && failText !== undefined) {
      message.error('failText');
    } else {
      message.error(res.data.message);
    }
  }
}

export const patchEncap = async (url, data, funcSuccess, successText, funcFail, failText) => {
  let res;
  await service.patch(url, data).then(result => {
    res = result;
  });

  if (res.data.code === 201) {
    // console.log('api:ok->' + JSON.stringify(res.data));
    if (funcSuccess) {
      funcSuccess(res.data);
    }
    if (successText !== null && successText !== '' && successText !== undefined) {
      message.success(successText);
    }
  } else {
    console.log('api:fali->' + JSON.stringify(res.data))
    if (funcFail) {
      funcFail(res.data);
    }
    if (failText !== null && failText !== '' && failText !== undefined) {
      message.error('failText');
    } else {
      message.error(res.data.message);
    }
  }
}

export const deleteEncap = async (url, data, funcSuccess, successText, funcFail, failText) => {
  let res;
  await service.delete(url, {
    data: data
  }).then(result => {
    res = result;
  });

  if (res.data.code === 201) {
    // console.log('api:ok->' + JSON.stringify(res.data));
    if (funcSuccess) {
      funcSuccess(res.data);
    }
    if (successText !== null && successText !== '' && successText !== undefined) {
      message.success(successText);
    }
  } else {
    console.log('api:fali->' + JSON.stringify(res.data))
    if (funcFail) {
      funcFail(res.data);
    }
    if (failText !== null && failText !== '' && failText !== undefined) {
      message.error('failText');
    } else {
      message.error(res.data.message);
    }
  }
}

// Urls
export const BlogsUrl = '/blogs'
export const FootprintsUrl = '/footprints'
export const PointsUrl = '/points'
export const TagsUrl = '/tags'
